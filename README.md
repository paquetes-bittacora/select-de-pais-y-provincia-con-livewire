Paquete para crear una pareja de campos país/provincia con una base de datos de países y provincias preinstalada.

# Instalación por comando

Ejecutar el comando `php artisan livewire-country-state-selector:install`.

# Instalación manual

Ejecutar las migraciones:

```
php artisan migrate
```

Ejecutar los seeders:

```
php artisan db:seed --class=\\Bittacora\\LivewireCountryStateSelector\\Database\\Seeders\\CountriesSeeder
php artisan db:seed --class=\\Bittacora\\LivewireCountryStateSelector\\Database\\Seeders\\StatesSeeder
```

# Uso

El paquete incluye 2 componentes de livewire, `country-select` y `state-select`, que se pueden usar de la siguiente forma:

```php
@livewire('country-select', ['selectedCountry' => $country, 'fieldName' => 'country'])
@livewire('state-select', ['selectedCountry' => $country, 'selectedState' => $state, 'fieldName' => 'state'])
```

Los valores de `selectedCountry` y `selectedState` serán los ids del país y provincia seleccionados si los hay, pero se pueden dejar vacíos para que no se muestre ningún valor seleccionado. 

`fieldName` establecerá el valor de `name` e `id` para cada campo.

El paquete incluye un html muy básico para cada campo para poder adaptarlo a cualquier plantilla, tanto en bPanel como en la parte pública.

# Notas

Basado en la base de datos de https://github.com/ponceelrelajado/paises_estados_del_mundo, con algunas correcciones de formato. 

Pueden encontrarse en inglés en
https://github.com/hiiamrohit/Countries-States-Cities-database.
