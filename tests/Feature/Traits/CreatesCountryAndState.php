<?php

declare(strict_types=1);

namespace Bittacora\LivewireCountryStateSelector\Tests\Feature\Traits;

use Bittacora\LivewireCountryStateSelector\Models\Country;
use Bittacora\LivewireCountryStateSelector\Models\State;

trait CreatesCountryAndState
{
    private function createCountryAndState(): void
    {
        $country = new Country();
        $country->name = 'Prueba';
        $country->save();

        $state = new State();
        $state->name = 'Prueba';
        $state->country_id = $country->id;
        $state->save();
    }
}
