<?php

declare(strict_types=1);

namespace Bittacora\LivewireCountryStateSelector\Tests\Feature\Factories;

use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Bittacora\LivewireCountryStateSelector\Models\Country;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\WithFaker;

final class AddressFactory
{
    use WithFaker;

    public function __construct()
    {
        $this->setUpFaker();
    }

    public function getAddress(
        Model $addressable,
        Country $country = null
    ): ModelAddress {
        if (null === $country) {
            $country = (new CountryFactory())->getCountry();
        }
        $address = new ModelAddress();
        $address->addressable()->associate($addressable);
        $address->country()->associate($country);
        $address->postal_code = $this->faker->numberBetween(10000, 99999);
        $address->location = $this->faker->city;
        $address->save();
        return $address;
    }
}
