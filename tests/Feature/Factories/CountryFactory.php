<?php

declare(strict_types=1);

namespace Bittacora\LivewireCountryStateSelector\Tests\Feature\Factories;

use Bittacora\LivewireCountryStateSelector\Models\Country;
use Illuminate\Foundation\Testing\WithFaker;

final class CountryFactory
{
    use WithFaker;

    public function __construct()
    {
        $this->setUpFaker();
    }

    public function getCountry(): Country
    {
        $country = new Country();
        $country->setName($this->faker->name());
        $country->save();
        return $country;
    }
}
