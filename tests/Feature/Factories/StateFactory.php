<?php

declare(strict_types=1);

namespace Bittacora\LivewireCountryStateSelector\Tests\Feature\Factories;

use Bittacora\LivewireCountryStateSelector\Models\Country;
use Bittacora\LivewireCountryStateSelector\Models\State;

final class StateFactory
{
    public function getStateId(): int
    {
        return $this->getState()->getId();
    }

    public function getState(): State
    {
        $country = new Country();
        $country->setName('País');
        $country->save();
        $state = new State();
        $state->setName('Provincia');
        $state->setCountryId($country->getId());
        $state->save();
        return $state;
    }
}
