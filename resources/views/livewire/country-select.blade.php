<div>
    <select
        wire:change="$emit('updateSelectedCountry')"
        wire:model="selectedCountry"
        name="{{ $fieldName }}"
        id="{{ $fieldName }}"
        class="{{ $class }}"
    >
        <option value="">Seleccione un país</option>
        @foreach($countries as $country)
            <option value="{{ $country->id }}" >{{ $country->name }}</option>
        @endforeach
    </select>
</div>
