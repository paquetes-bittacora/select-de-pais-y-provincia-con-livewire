<div>
    <select
        wire:model="selectedState"
        name="{{ $fieldName }}"
        id="{{ $fieldName }}"
        class="{{ $class }}"
    >
        @if ($selectedCountry === 0)
            <option value="">Primero debe seleccionar un país</option>
        @else
            <option value="">Seleccione una provincia</option>
            @foreach($states as $state)
                <option value="{{ $state->id }}" @if($state->id === $selected)selected="selected"@endif>{{ $state->name }}</option>
            @endforeach
        @endif
    </select>
</div>
