<?php

declare(strict_types=1);

namespace Bittacora\LivewireCountryStateSelector;

use Bittacora\LivewireCountryStateSelector\Commands\InstallCommand;
use Bittacora\LivewireCountryStateSelector\Http\Livewire\CountrySelect;
use Bittacora\LivewireCountryStateSelector\Http\Livewire\StateSelect;
use Illuminate\Support\ServiceProvider;
use Livewire;

class LivewireCountryStateSelectorServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        if ($this->app->runningInConsole()) {
            $this->commands([InstallCommand::class]);
        }

        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadViewsFrom(__DIR__ . '/../resources/views/livewire', 'location-selectors');

        $this->registerLivewireComponents();
    }

    private function registerLivewireComponents(): void
    {
        Livewire::component('country-select', CountrySelect::class);
        Livewire::component('state-select', StateSelect::class);
    }
}
