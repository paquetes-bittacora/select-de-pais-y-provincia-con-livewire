<?php

namespace Bittacora\LivewireCountryStateSelector\Http\Livewire;

use Bittacora\LivewireCountryStateSelector\Models\State;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;
use Livewire\Component;

class StateSelect extends Component
{
    public int $selected = 0;

    public int $selectedCountry = 0;

    public int $selectedState = 0; // Se usa para establecer el estado inicial

    public string $fieldName = 'state';

    public string $class = '';

    /** @var Collection<int, State>  */
    public Collection $states;

    protected $listeners = ['countryHasChanged' => 'updateStates'];

    public function mount(): void
    {
        $this->updateStates($this->selectedCountry);
    }

    public function render(): Factory|View|Application
    {
        return view('location-selectors::state-select');
    }

    public function updateStates(int $countryId): void
    {
        $this->selectedCountry = $countryId;
        $this->states = State::where('country_id', '=', $this->selectedCountry)->orderBy('name')->get();
    }
}
