<?php

namespace Bittacora\LivewireCountryStateSelector\Http\Livewire;

use Bittacora\LivewireCountryStateSelector\Models\Country;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;
use Livewire\Component;

class CountrySelect extends Component
{
    public int $selectedCountry = 28;

    /** @var Collection<int, Country> */
    public Collection $countries;

    public string $fieldName = 'country';

    public string $class = '';

    protected $listeners = ['updateSelectedCountry' => 'updateSelectedCountry'];

    public function mount(): void
    {
        $this->countries = Country::orderBy('name')->get();
    }

    public function render(): Application|Factory|View
    {
        return view('location-selectors::country-select');
    }

    public function updateSelectedCountry()
    {
        $this->emit('countryHasChanged', $this->selectedCountry);
    }
}
