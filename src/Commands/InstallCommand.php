<?php

declare(strict_types=1);

namespace Bittacora\LivewireCountryStateSelector\Commands;

use Artisan;
use Illuminate\Console\Command;

class InstallCommand extends Command
{

    protected $signature = 'livewire-country-state-selector:install';

    protected $description = 'Instala el paquete livewire-country-state-selector';

    public function handle(): void
    {
        echo "Instalando livewire-country-state-selector...\n";

        Artisan::call('db:seed', [
            '--class' => '\\Bittacora\\LivewireCountryStateSelector\\Database\\Seeders\\CountriesSeeder',
            '--force' => true,
        ]);
        Artisan::call('db:seed', [
            '--class' => '\\Bittacora\\LivewireCountryStateSelector\\Database\\Seeders\\StatesSeeder',
            '--force' => true,
        ]);
        echo "Hecho\n";
    }
}
