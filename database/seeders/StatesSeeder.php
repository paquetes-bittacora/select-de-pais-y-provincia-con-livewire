<?php

namespace Bittacora\LivewireCountryStateSelector\Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = file_get_contents(__DIR__ . '/../sql/estado.sql');
        DB::unprepared($sql);
    }
}
