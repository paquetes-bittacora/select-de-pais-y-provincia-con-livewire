<?php

declare(strict_types=1);

namespace Bittacora\LivewireCountryStateSelector\Database\Factories;

use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Bittacora\LivewireCountryStateSelector\Models\Country;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<ModelAddress>
 */
final class CountryFactory extends Factory
{
    /** @var string  */
    protected $model = Country::class;

    /**
     * @return array<string, int>|array<string, string>|array<string, mixed[]>
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->name,
        ];
    }
}
