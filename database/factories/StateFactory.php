<?php

declare(strict_types=1);

namespace Bittacora\LivewireCountryStateSelector\Database\Factories;

use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Bittacora\LivewireCountryStateSelector\Models\State;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<ModelAddress>
 */
final class StateFactory extends Factory
{
    /** @var string  */
    protected $model = State::class;

    /**
     * @return array<string, string>|array<string, \Bittacora\LivewireCountryStateSelector\Database\Factories\CountryFactory>
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->name,
            'country_id' => (new CountryFactory()),
        ];
    }
}
